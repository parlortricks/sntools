#  The top-level makefile simply invokes all the other makefiles

prefix = $(HOME)/.local

PROGRAMS = sne snp

all: $(PROGRAMS)

bin/%:
	mkdir -p bin
	$(MAKE) -C$* $*
	cp $*/$* $@

doc/%.1:
	mkdir -p doc
	cp $*/$*.1 $@

sne: bin/sne doc/sne.1
snp: bin/snp doc/snp.1

install: $(PROGRAMS)
	mkdir -p $(prefix)/bin
	cp bin/* $(prefix)/bin/.
	mkdir -p $(prefix)/share/man/man1
	cp doc/* $(prefix)/share/man/man1/.

clean:
	for dir in $(PROGRAMS) ; do $(MAKE) -C$$dir clean ; done
	rm -f $(PROGRAMS:%=bin/%) $(PROGRAMS:%=doc/%.1)
