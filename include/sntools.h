/* See LICENSE file for copyright and license details. */
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

const char MAGIC[] = {'D', 'A', 'T', 0x1a};

typedef struct {
    char magic[sizeof(MAGIC)];
    uint16_t num_items;
    uint32_t ofs_index;
    uint32_t len_index;
} Header;

typedef struct {
    char name[16];
    uint32_t len_body;
    uint32_t ofs_body;
} Entry;

typedef struct {
    Entry *entries; 
} Index;

uint16_t read_uint16_le(FILE *file) {
    uint8_t bytes[2];
    if (fread(bytes, sizeof(uint8_t), 2, file) != 2) {
        fprintf(stderr, "Failed to read 2 bytes\n");
        exit(1);
    }

    return (uint16_t)bytes[0] | ((uint16_t)bytes[1] << 8);
}

uint32_t read_uint32_le(FILE *file) {
    uint8_t bytes[4];
    if (fread(bytes, sizeof(uint8_t), 4, file) != 4) {
        fprintf(stderr, "Failed to read 4 bytes\n");
        exit(1);
    }

    return (uint32_t)bytes[0] | ((uint32_t)bytes[1] << 8) | ((uint32_t)bytes[2] << 16) | ((uint32_t)bytes[3] << 24);
}