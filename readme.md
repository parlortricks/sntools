# Shaw's Nightmare Tools
These tools have been re-written in [C99](https://en.wikipedia.org/wiki/C99) from [Fennel](https://fennel-lang.org/) and packaged together as a set of tools.

 - SNE (Shaw's Nightmare Extracter)
 - SNP (Shaw's Nightmare Packer)

You can get [Shaw's Nightmare](https://shawsnightmare.ucoz.com/) from [https://shawsnightmare.ucoz.com/](https://shawsnightmare.ucoz.com/). I am using the latest version SN.DAT from SNFP.ZIP.

# Usage
    sne <archive>

# Attributions
Thanks to Mickey Productions for their help. 

# License
MIT LICENSE

2023 parlortricks <parlortricks@fastmail.fm>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the \"Software\"), to
deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
IN THE SOFTWARE.