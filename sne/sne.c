/* See LICENSE file for copyright and license details. */
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <sys/stat.h>
#include <errno.h>

#include "sntools.h"

static char const *help =
"Shaw's Nightmare Extractor\n\n"
"Usage: sne <archive>\n"
"Extract all files to the directory 'extract'.\n\n"
"  -h,      Display this help\n"
"  -l,      Display contents of archive\n"
"  -v,      Display the version\n";

static char const *version =
"sne, version 1.0\n"
"https://codeberg.org/parlortricks/sntools\n\n"
"MIT LICENSE\n"
"https://spdx.org/licenses/MIT\n"

"2023 parlortricks <parlortricks@fastmail.fm>\n\n"

"Permission is hereby granted, free of charge, to any person obtaining a copy\n"
"of this software and associated documentation files (the \"Software\"), to\n"
"deal in the Software without restriction, including without limitation the\n" 
"rights to use, copy, modify, merge, publish, distribute, sublicense, and/or\n" 
"sell copies of the Software, and to permit persons to whom the Software is\n" 
"furnished to do so, subject to the following conditions:\n\n"

"The above copyright notice and this permission notice shall be included in\n" 
"all copies or substantial portions of the Software.\n\n"

"THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS\n" 
"OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,\n" 
"FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE\n" 
"AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER\n" 
"LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING\n" 
"FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS\n"
"IN THE SOFTWARE.\n";

void print_help() {
    printf("%s\n", help);
}

void print_version() {
    printf("%s\n", version);
}

int main(int argc, char *argv[]) {
    if (argc < 2) {
        print_help(argv[0]);
        return 1;
    }

    for (int i = 1; i < argc; i++) {
        if (strcmp(argv[i], "-h") == 0) {
            print_help();
            return 0;
        } else if (strcmp(argv[i], "-v") == 0) {
            print_version();
            return 0;
        }
    }

    FILE *file = fopen(argv[1], "rb");
    if (file == NULL) {
        if (errno == ENOENT) {
            printf("File does not exist: %s\n", argv[1]);
        } else {
            perror("Failed to open file");
        }
        return 1;
    }
    Header header;

    if (fread(&header.magic, sizeof(header.magic), 1, file) != 1) {
        printf("Failed to read file or file is too short\n");
        fclose(file);
        return 1;
    }

    if(memcmp(MAGIC, header.magic, sizeof(MAGIC)) != 0) {
        printf("Magic doesn't match!\n");
        fclose(file);
        return 1;
    }

    header.num_items = read_uint16_le(file);
    header.ofs_index = read_uint32_le(file);
    header.len_index = read_uint32_le(file);

    int magic_size = sizeof(header.magic);
    printf("Magic: [%.*s]\nNumber of items: %u\nOffset index: %u\nLength index: %u\n", 
        magic_size, 
        header.magic, 
        header.num_items, 
        header.ofs_index, 
        header.len_index);

    uint32_t offset = header.ofs_index;

    Index index;
    index.entries = malloc(header.num_items * sizeof(Entry));

    if (index.entries == NULL) {
        printf("Failed to allocate memory\n");
        fclose(file);
        return 1;
    }
    
    for (int i = 0; i < header.num_items; i++) {
        if (fseek(file, offset, SEEK_SET) != 0) {
            printf("Failed to seek to offset\n");
            exit(1);
        }

        Entry entry;
        
        if (fread(&entry, sizeof(Entry), 1, file) != 1) {
            printf("Failed to read file or file is too short\n");
            fclose(file);
            return 1;
        }

        index.entries[i] = entry;
        offset += sizeof(entry);
    }

    const char *dirpath = "extract";

    if (mkdir(dirpath, 0777) == -1) {
        if (errno != EEXIST) {
            perror("Error creating directory");
            return 1;
        }
    }

    printf("Directory %s either already exists or was created successfully\n", dirpath);
  
    char filepath[256];
    for(int i=0; i < header.num_items; i++) {
        printf("name: %s\nlen_body: %u\nofs_body: %u\n", 
            index.entries[i].name,
            index.entries[i].len_body,
            index.entries[i].ofs_body);

        char contents[index.entries[i].len_body];
        
        if (fseek(file, index.entries[i].ofs_body, SEEK_SET) != 0) {
            printf("Failed to seek to offset\n");
            exit(1);
        }

        if (fread(&contents, sizeof(contents), 1, file) != 1) {
            printf("Failed to read file or file is too short\n");
            fclose(file);
            return 1;
        }

        snprintf(filepath, sizeof(filepath), "%s/%s", dirpath, index.entries[i].name);
        FILE *new_file = fopen(filepath, "wb");
        if (new_file == NULL) {
            printf("Failed to open file\n");
            return 1;
        }
        size_t result = fwrite(contents, sizeof(char), sizeof(contents), new_file);
        if (result != sizeof(contents)) {
            printf("Error writing to file\n");
            return 1;
        }

        fclose(new_file);
    }

    free(index.entries);

    fclose(file);
    return 0;
}
