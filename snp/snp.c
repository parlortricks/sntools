/* See LICENSE file for copyright and license details. */
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <sys/stat.h>
#include <errno.h>

#include "sntools.h"

static char const *help =
"Shaw's Nightmare Packer\n\n"
"Usage: snp <archive> FILES...\n"
"Pack all files to the <archive>.\n\n"
"  -h,      Display this help\n"
"  -v,      Display the version\n";

static char const *version =
"snp, version 1.0\n"
"https://codeberg.org/parlortricks/sntools\n\n"
"MIT LICENSE\n"
"https://spdx.org/licenses/MIT\n"

"2023 parlortricks <parlortricks@fastmail.fm>\n\n"

"Permission is hereby granted, free of charge, to any person obtaining a copy\n"
"of this software and associated documentation files (the \"Software\"), to\n"
"deal in the Software without restriction, including without limitation the\n" 
"rights to use, copy, modify, merge, publish, distribute, sublicense, and/or\n" 
"sell copies of the Software, and to permit persons to whom the Software is\n" 
"furnished to do so, subject to the following conditions:\n\n"

"The above copyright notice and this permission notice shall be included in\n" 
"all copies or substantial portions of the Software.\n\n"

"THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS\n" 
"OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,\n" 
"FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE\n" 
"AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER\n" 
"LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING\n" 
"FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS\n"
"IN THE SOFTWARE.\n";

void print_help() {
    printf("%s\n", help);
}

void print_version() {
    printf("%s\n", version);
}

int main(int argc, char *argv[]) {
    if (argc < 2) {
        print_help(argv[0]);
        return 1;
    }

    for (int i = 1; i < argc; i++) {
        if (strcmp(argv[i], "-h") == 0) {
            print_help();
            return 0;
        } else if (strcmp(argv[i], "-v") == 0) {
            print_version();
            return 0;
        }
    }

    return 0;
}
